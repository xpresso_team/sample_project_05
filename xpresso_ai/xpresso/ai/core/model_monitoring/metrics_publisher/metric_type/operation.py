"""
    Module that defines Model operation schema
"""

__all__ = ["Operation"]
__author__ = ["Shlok Chaudhari"]

from xpresso.ai.core.commons.utils.xpresso_base_enum import BaseEnum


class Operation(BaseEnum):
    """
        Class that lists down all the labels
        for model operation schema
    """
    MODEL_ID = "model_id"
    TYPE = "type"
    TIME = "time"
    TOTAL_CORE = "total_core"
    CPU_USAGE_PC = "cpu_usage_pc"
    MEM_USAGE_BYTES = "mem_usage_bytes"
    MEM_TOTAL_BYTES = "mem_total_bytes"
    TOTAL_GPU = "total_gpu"
    GPU_MEM_USAGE_BYTES = "gpu_mem_usage_bytes"
    GPU_MEM_TOTAL_BYTES = "gpu_mem_total_bytes"
    ROOT_DISK_USAGE_BYTES = "root_disk_usage_bytes"
    ROOT_DISK_TOTAL_BYTES = "root_disk_total_bytes"
    TOP_PARTITION_DISK_USAGE_BYTES = "top_partition_disk_usage_bytes"
    TOP_PARTITION_DISK_TOTAL_BYTES = "top_partition_disk_total_bytes"
